import { createGlobalStyle } from 'styled-components';
 
const GlobalStyle = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
        background: black;
        color: white;
    }

    html,
    body,
    .root {
        height:100%
    }
`;
 
export default GlobalStyle;