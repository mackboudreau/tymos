import React, { useState } from 'react';
import * as yup from 'yup';
import { Redirect, useHistory } from "react-router-dom";

import { login, isLoggedIn } from 'common/auth';
import {
    PASSWORD_TOO_SHORT,
    PASSWORD_TOO_LONG,
    PASSWORD_REQUIRED,
    EMAIL_REQUIRED,
    INVALID_EMAIL,
} from 'common/constants';
import Page from 'components/Page';
import Input from 'components/Input';
import Button from 'components/Button';

import * as Styled from './styles';

function Login() {
    const [errMessage, setErrMessage] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const schema = yup.object().shape({
        email: yup.string().email(INVALID_EMAIL).required(EMAIL_REQUIRED),
        password: yup.string().required(PASSWORD_REQUIRED).min(5, PASSWORD_TOO_SHORT).max(15, PASSWORD_TOO_LONG),
    });

    const history = useHistory();
    const attemptLogin = (e) => {
        e.preventDefault();
        schema.validate({
            email: email,
            password: password,
        }).catch(function (err) {
            // Validate email and passwords length > 5 and < 15
            if (err.errors.length > 0) {
                setErrMessage(err.errors[0]);
            }
        }).then((value) => {
            if (value) {
                // Clear any remaining error messages
                setErrMessage("");
                // Login user
                if (login(email)) {
                    // Redirect user
                    history.push("/private");
                };
            }
        });
    }

    return isLoggedIn() ? <Redirect to={{ pathname: '/private'}} /> : (
        <Page>
            <Styled.Title>Welcome back!</Styled.Title>
            {errMessage && (
                <Styled.Error>
                    <img alt="Warning symbol" src="/icons/warning.svg" />
                    {errMessage}
                </Styled.Error>
                )
            }
            <Styled.Form novalidate onSubmit={attemptLogin}> 
                <Input
                    type="text"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    placeholder="Email"
                />
                <Input
                    type="password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    placeholder="Password"
                />
                <Styled.ButtonContainer>
                    <Button>Login</Button>
                </Styled.ButtonContainer>
            </Styled.Form>
        </Page>
    );
}

export default Login;