import styled from 'styled-components';

export const Title = styled.h1`
    text-align: center;
`;

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;

    > :not(:last-child) {
        margin-bottom: 16px;
    }
`;

export const ButtonContainer = styled.div`
    display: flex;
    justify-content: end;
    width: 350px;
`;

export const Error = styled.p`
    text-align: center;
    font-size: 14px;
    color: red;

    > img {
        margin-bottom: -3px;
        margin-right: 5px;
    }
`;