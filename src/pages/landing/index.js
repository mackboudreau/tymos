import React from 'react';
import { Link, Redirect } from "react-router-dom";

import { isLoggedIn } from 'common/auth';
import Page from 'components/Page';
import Button from 'components/Button';

import * as Styled from './styles';

function Landing() {
    return isLoggedIn() ? <Redirect to={{ pathname: '/private'}} /> : (
        <Page>
            <Styled.Hero>
                <Styled.HeroText>
                    <Styled.HeroTitle>Travel to any time at lightning fast speeds.</Styled.HeroTitle>
                    <Styled.HeroDescription>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi non sem enim. Morbi eu
                        nulla in erat cursus bibendum. In a suscipit ex.
                    </Styled.HeroDescription>
                    <Link to="/login">
                        <Button>Login</Button>
                    </Link>
                </Styled.HeroText>
                <Styled.HeroImage src="/images/time1.jpg" />
            </Styled.Hero>
            <Styled.Info>
                <Styled.InfoTitle>Lorem ipsum dolor sit amet</Styled.InfoTitle>
                <Styled.InfoDescription>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi non sem enim. Morbi eu
                    nulla in erat cursus bibendum. In a suscipit ex. Donec sodales eget diam nec pellentesque.
                        Aliquam quis sem vitae urna tincidunt auctor. Mauris varius tempus ex, nec euismod nisl faucibus et.
                        Quisque congue varius leo. Nullam sed dictum metus. Donec iaculis laoreet elit non molestie.
                </Styled.InfoDescription>
            </Styled.Info>
            <Styled.Previews>
                <Styled.PreviewsItem src="/images/time2.jpg" />
                <Styled.PreviewsItem src="/images/time3.jpg" />
                <Styled.PreviewsItem src="/images/time4.jpg" />
            </Styled.Previews>
        </Page>
    );
}

export default Landing;