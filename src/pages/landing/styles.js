import styled from 'styled-components';

const DEVICE_MOBILE = "(max-width: 1190px)";

export const Hero = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    @media ${DEVICE_MOBILE} { 
        flex-direction: column;
    }
`;

export const HeroText = styled.div`
    max-width: 650px;
    margin-right: 25px;

    @media ${DEVICE_MOBILE} { 
        max-width: 100%;
        margin-top: 25px;
        order: 1;
    }
`;

export const HeroTitle = styled.h1`
    font-size: 36px;
    margin: 0;
`;

export const HeroDescription = styled.p`
    margin-top: 10px;
    margin-bottom: 30px;
`;

export const HeroImage = styled.img`
    max-width: 600px;
    border-radius: 8px;

    @media ${DEVICE_MOBILE} { 
        width: 100%;
        max-width: 100%;
    }
`;

export const Info = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 100px;
    margin-bottom: 25px;
    text-align: center;
`;

export const InfoTitle = styled.h1`
    margin: 0;
`;

export const InfoDescription = styled.p`
    max-width: 900px;
`;

export const Previews = styled.div`
    display: flex;
    justify-content: center;
    margin-bottom: 50px;

    @media (max-width: 1100px) { 
        flex-direction: column;
    }
`;

export const PreviewsItem = styled.img`
    width: 350px;
    height: 200px;
    object-fit: cover;
    border-radius: 8px;

    &:not(:first-child) {
        margin-left: 16px;
    }

    @media (max-width: 1100px) { 
        width: 100%;

        &:not(:first-child) {
            margin-top: 16px;
            margin-left: 0;
        }
    }
`;