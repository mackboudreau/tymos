import React, { useState } from 'react';
import { Redirect } from "react-router-dom";
import { CSSTransition, SwitchTransition } from 'react-transition-group';

import "./react-datepicker.css";

import Page from 'components/Page';
import Button from 'components/Button';
import { isLoggedIn } from 'common/auth';

import * as Styled from './styles';

function Private() {
    const [newDate, setNewDate] = useState(new Date());
    const [currentDate, setcurrentDate] = useState(new Date());

    const date = new Date(currentDate);

    const timeTravel = (e) => {
        e.preventDefault();
        setcurrentDate(newDate);
    }

    return !isLoggedIn() ? <Redirect to={{ pathname: '/login'}} /> : (
        <Page>
            <Styled.Console>
                <h1>Time Travel Console</h1>
                <Styled.DateDisplayLabel>Current Date:</Styled.DateDisplayLabel>
                <SwitchTransition mode="out-in">
                    <CSSTransition
                    key={date.toLocaleDateString()}
                    addEndListener={(node, done) => node.addEventListener("transitionend", done, false)}
                    classNames='fade'
                    >
                    <Styled.DateDisplay>{ date.toLocaleDateString() }</Styled.DateDisplay>
                    </CSSTransition>
                </SwitchTransition>
                <Styled.DateDisplayLabel>Destination Date:</Styled.DateDisplayLabel>
                <Styled.Form onSubmit={timeTravel}>
                    <Styled.DateInput selected={newDate} onChange={(date) => setNewDate(date)} />
                    <Button>Time Travel</Button>
                </Styled.Form>
            </Styled.Console>
        </Page>
    );
}

export default Private;