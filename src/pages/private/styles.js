import styled from 'styled-components';
import DatePicker from "react-datepicker";

export const Console = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;

    .fade-enter {
        background-color: white;
     }
     .fade-exit {
        background-color: rgba(255, 255, 255, 0.075);
     }
     .fade-enter-active {
        background-color: rgba(255, 255, 255, 0.075);
     }
     .fade-exit-active {
        background-color: white;
     }
     .fade-enter-active,
     .fade-exit-active {
        transition: all 200ms;
     }
`;

export const DateDisplayLabel = styled.label`
    font-size: 14px;
    color: rgba(255, 255, 255, 0.3);
`;

export const DateDisplay = styled.span`
    font-size: 26px;
    letter-spacing: 12px;
    font-family: "Lucida Console", "Courier New", monospace;
    display: flex;
    cursor: default;
    margin-top: 4px;
    margin-bottom: 12px;
    background-color: rgba(255, 255, 255, 0.075);
    padding: 12px 30px;
    border-radius: 8px;
`;

export const Form = styled.form`
    max-width: 352px;
`;

export const DateInput = styled(DatePicker)`
    margin-left: 0;
    margin-right: 0;
    margin-bottom: 18px;
    margin-top: 4px;
    padding: 16px 26px;
    min-width: 300px;
    background-color: black;
    color: white;
    border: none;
    border-radius: 6px;
    outline: 1px solid #4E4E4E;
    transition: all 0.3s ease;

    &:focus {
        outline: none;
        outline: 1px solid #F63030;
    }
`;