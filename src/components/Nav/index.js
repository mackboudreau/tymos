import React from 'react';
import { useHistory } from "react-router-dom";

import { isLoggedIn, logout } from 'common/auth';
import * as Styled from './styles';

const ACTIVE_STYLE = {fontWeight: "bold", opacity: 1};

function Nav() {
    const history = useHistory();

    const attemptLogout = () => {
        if (logout()) {
            history.push("/login");
        }
    };

    const loggedIn = isLoggedIn();
    return (
        <Styled.Nav>
            {
                loggedIn ? (
                    <>
                        <Styled.NavButton onClick={attemptLogout}>Logout</Styled.NavButton>
                        <Styled.Profile>
                            <img alt="User Avatar" src="/images/random_face.png" />
                            <p>{loggedIn}</p>        
                        </Styled.Profile>
                    </>
                ) : (
                    <>
                        <Styled.NavItem exact to="/" activeStyle={ACTIVE_STYLE}>Home</Styled.NavItem>
                        <Styled.NavItem to="/login" activeStyle={ACTIVE_STYLE}>Login</Styled.NavItem>
                    </>
                )
            }
        </Styled.Nav>
    );
}


export default Nav;