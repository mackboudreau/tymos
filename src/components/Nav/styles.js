import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const Nav = styled.nav`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const NavItem = styled(NavLink)`
    color: white;
    text-decoration: none;
    opacity: 0.5;
    transition: opacity 0.1s;

    &:not(:first-child) {
        margin-left: 40px;
    }

    &:hover {
        opacity: 1;
    }
`;

export const Profile = styled.div`
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: 40px;

    > img {
        height: 25px;
        width: 25px;
        border-radius: 12.5px;
        margin-right: 8px;
    }

    > p {
        margin: 0;
    }
`;

export const NavButton = styled.a`
    color: white;
    cursor: pointer;
    text-decoration: none;
    opacity: ${props => props.active ? '1' : '0.5'};
    transition: opacity 0.1s;

    &:hover {
        opacity: 1;
    }
`;