import styled from 'styled-components';

export const Input = styled.input`
    margin: 0;
    padding: 16px 26px;
    min-width: 300px;
    background-color: black;
    color: white;
    border: none;
    border-radius: 6px;
    outline: 1px solid #4E4E4E;
    transition: all 0.3s ease;

    &:focus {
        outline: none;
        outline: 1px solid #F63030;
    }
`;