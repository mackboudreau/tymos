import GlobalStyle from 'globalStyles';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Landing from 'pages/landing';
import Login from 'pages/login';
import Private from 'pages/private';

function App() {
  return (
    <Router>
      <GlobalStyle />
      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/private">
          <Private />
        </Route>
        <Route path="/">
          <Landing />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
