import styled from 'styled-components';
import { COLORS } from "common/theme";

export const Button = styled.button`
    padding: 14px 46px;
    background-color: ${COLORS.RED};
    border-radius: 6px;
    color: white;
    cursor: pointer;
    text-decoration: none;
    font-size: 14px;
    font-weight: bold;
    transition: all 0.1s ease;
    border: none;

    &:hover {
        background-color: #FC2222;
    }

    &:active {
        background-color: #D10E0E;
    }
`;