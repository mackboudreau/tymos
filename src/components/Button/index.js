import React from 'react';

import * as Styled from './styles';

function Button(props) {
    return (
        <Styled.Button {...props } />
    );
}

export default Button;