import styled from 'styled-components';

export const Footer = styled.div`
    display: flex;
    opacity: 0.5;
    justify-content: center;
    font-size: 12px;
    padding: 25px 0;
`;