import React from 'react';

import * as Styled from './styles';

function Footer() {
    return (
        <Styled.Footer>
            Tymos @ 2021
        </Styled.Footer>
    );
}

export default Footer;