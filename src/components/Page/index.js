import React from 'react';

import Header from 'components/Header';
import Footer from 'components/Footer';

import * as Styled from './styles';

function Page(props) {
  return (
    <Styled.Page>
        <Header />
        {props.children}
        <Footer />
    </Styled.Page>
  );
}

export default Page;