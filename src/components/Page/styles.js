import styled from 'styled-components';

export const Page = styled.div`
    padding: 0 100px;
    height: 100%;
`;