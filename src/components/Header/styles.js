import styled from 'styled-components';

export const Header = styled.header`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding-top: 25px;
    padding-bottom: 25px;
    margin-bottom: 50px;
`;

export const Title = styled.img`
    margin: 0;
`;