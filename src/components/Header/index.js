import React from 'react';

import Nav from 'components/Nav';
import { Link } from "react-router-dom";

import * as Styled from './styles';

function Header() {
    return (
        <Styled.Header>
            <Link to="/">
                <Styled.Title src="/images/logo.svg" />
            </Link>
            <Nav />
        </Styled.Header>
    );
}

export default Header;