// Validation Error Messages
export const PASSWORD_TOO_SHORT = "Please choose a longer password. Passwords must be between 5 and 15 characters.";
export const PASSWORD_TOO_LONG = "Please choose a shorter password. Passwords must be between 5 and 15 characters.";
export const PASSWORD_REQUIRED = "Please enter a password.";
export const EMAIL_REQUIRED = "Please enter an email.";
export const INVALID_EMAIL = "Please enter a valid email.";

// Localstorage Keys
export const USER_EMAIL_SESSION = "user_email_session"