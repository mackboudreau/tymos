import { USER_EMAIL_SESSION } from 'common/constants';

export function login(email) {
    localStorage.setItem(USER_EMAIL_SESSION, email);
    return true;
}

export function logout() {
    localStorage.removeItem(USER_EMAIL_SESSION);
    return true;
}

export function isLoggedIn() {
    // Check to see if there's active user session
    const emailSession = localStorage.getItem(USER_EMAIL_SESSION);
    return emailSession;
}