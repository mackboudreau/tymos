<p align="center"><img src="public/images/logo_dark.svg"></p>

# Tymos
A single page react app for BYOS.

# Usage
Run `npm i && npm start` to install dependencies and start the react app or use `docker run -p 3000:80 -d mackenzieboudreau/tymos:0.0.1` to pull the container from dockerhub.

# Libraries used
- Yup
- React-datepicker 
- React-transition-group