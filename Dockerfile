# Build wepapp
FROM node:alpine3.11 as build

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN npm i
COPY . /app

RUN npm run build

# Run webapp
FROM nginx:1.21.3-alpine  
COPY --from=build /app/build /usr/share/nginx/html